const fibonacci = (n) => {
    if (fibArray[n]  === undefined)  {
        fibArray[n] = fibonacci(n-1) + fibonacci(n-2);
    }
    
    return fibArray[n];
};

let fibArray = [1,1];
fibonacci(1);
console.log(fibArray.slice(0,1));
fibonacci(10);
console.log(fibArray.slice(0,10));
